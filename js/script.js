window.addEventListener('DOMContentLoaded', (event) => {
  retta();
  iperbole();
  parabola();
  radiceQuadrata();
  esponenziale();
  logaritmica();
  hideControls();
});

function hideControls(){
    let buttons = document.getElementsByClassName('modebar-btn');
    for(let i = 0; i < buttons.length; i++){
        buttons[i].classList.add("d-none");
    }
    let buttonsShow = document.querySelectorAll("[data-title='Download plot as a png']");
    for(let i = 0; i < buttonsShow.length; i++){
        buttonsShow[i].classList.remove("d-none");
    }
    let buttonsShow2 = document.querySelectorAll("[data-title='Pan']");
    for(let i = 0; i < buttonsShow2.length; i++){
        buttonsShow2[i].classList.remove("d-none");
    }
    let buttonsShow3 = document.querySelectorAll("[data-title='Zoom in']");
    for(let i = 0; i < buttonsShow3.length; i++){
        buttonsShow3[i].classList.remove("d-none");
    }
    let buttonsShow4 = document.querySelectorAll("[data-title='Zoom out']");
    for(let i = 0; i < buttonsShow4.length; i++){
        buttonsShow4[i].classList.remove("d-none");
    }
    let buttonsShow5 = document.querySelectorAll("[data-title='Autoscale']");
    for(let i = 0; i < buttonsShow5.length; i++){
        buttonsShow5[i].classList.remove("d-none");
    }
}

function tableHeader(table){
    table.innerHTML = "";
    let newRow = table.insertRow();
    let xCell = newRow.insertCell();
    xCell.setAttribute("width","45%");
    xCell.appendChild(document.createTextNode("x"));
    let yCell = newRow.insertCell();
    yCell.setAttribute("width","45%");
    yCell.appendChild(document.createTextNode("y"));
    let actionCell = newRow.insertCell();
    actionCell.setAttribute("width","10%");
}

document.getElementById('button-export').addEventListener('click',function(e){
    const rows = [
        rettaX,
        iperboleX,
        parabolaX,
        radiceQuadrataX,
        esponenzialeX,
        logaritmicaX
    ];
    
    let csvContent = "data:text/csv;charset=utf-8," + rows.map(e => e.join(",")).join("\n");

    var encodedUri = encodeURI(csvContent);
    window.open(encodedUri);
});