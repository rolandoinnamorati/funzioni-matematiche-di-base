const iperboleX = [0];

function iperbole(newX = null){
  if(newX){
    if(newX > 0)
      iperboleX.push(parseInt(newX),parseInt(newX)-parseInt(newX)-parseInt(newX));
    else
      iperboleX.push(parseInt(newX),Math.abs(parseInt(newX)));
  }
  iperboleX.sort(function(a, b) {
    return a - b;
  });
  let y = [];
  iperboleX.forEach(val => {
      y.push(1/val); // Y = 1 / x
  });

  //Plot
  const iperbolePlot = {
    x: iperboleX,
    y: y,
    type: 'scatter'
  }
  const data = [iperbolePlot];
  Plotly.newPlot('chart-iperbole', data)

  let table = document.getElementById('table-iperbole');
  tableHeader(table);
  for(let i = 0; i < iperboleX.length; i++){
    let newRow = table.insertRow();
    let xCell = newRow.insertCell();
    xCell.appendChild(document.createTextNode(iperboleX[i]));
    let yCell = newRow.insertCell();
    yCell.appendChild(document.createTextNode(y[i]));
    let actionCell = newRow.insertCell();
    let button = document.createElement("button");
    button.classList.add("btn","btn-danger",'btn-sm','m-0');
    button.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16"><path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/></svg>';
    actionCell.appendChild(button);
  }
  hideControls();
}

function iperboleAdd(){
  let x = prompt('Inserisci valore x');
  if(!iperboleX.includes(parseInt(x))) iperbole(x);
}