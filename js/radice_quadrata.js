const radiceQuadrataX = [0,2,10];

function radiceQuadrata(newX = null){

  if(newX) radiceQuadrataX.push(parseInt(newX));
  radiceQuadrataX.sort(function(a, b) {
    return a - b;
  });
  let y = [];
  radiceQuadrataX.forEach(val => {
    y.push(Math.sqrt(val)); // Y = √x
  });
  
    //Plot
  const radiceQuadrataPlot = {
    x: radiceQuadrataX,
    y: y,
    type: 'scatter'
  }
  const data = [radiceQuadrataPlot];
  Plotly.newPlot('chart-radice-quadrata', data)
  
  let table = document.getElementById('table-radice-quadrata');
  tableHeader(table);
  for(let i = 0; i < radiceQuadrataX.length; i++){
    let newRow = table.insertRow();
    let xCell = newRow.insertCell();
    xCell.appendChild(document.createTextNode(radiceQuadrataX[i]));
    let yCell = newRow.insertCell();
    yCell.appendChild(document.createTextNode(y[i]));
    let actionCell = newRow.insertCell();
    let button = document.createElement("button");
    button.classList.add("btn","btn-danger",'btn-sm','m-0');
    button.setAttribute("id",radiceQuadrataX[i]);
    button.setAttribute("onclick","radiceQuadrataDelete("+radiceQuadrataX[i]+")");
    button.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16"><path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/></svg>';
    actionCell.appendChild(button);
  }
  hideControls();
  }
  
function radiceQuadrataAdd(){
  let x = prompt('Inserisci valore x');
  if(!radiceQuadrataX.includes(parseInt(x)) & x >= 0) radiceQuadrata(x);
}

function radiceQuadrataDelete(x){
  let index = radiceQuadrataX.indexOf(x);
  if(index >= 0)
    radiceQuadrataX.splice(index, 1);
    radiceQuadrata();
}