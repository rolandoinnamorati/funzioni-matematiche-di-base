const rettaX = [];

function retta(){
    const m =  parseInt(document.getElementById('input-retta-m').value); // Coefficiente angolare
    const q = parseInt(document.getElementById('input-retta-q').value); // Termine noto
    rettaX.sort(function(a, b) {
      return a - b;
    });;
    let y = [];
    rettaX.forEach(val => {
        y.push(val*m+q); // Y = mx + q
    });
  
    const rettaPlot = {
      x: rettaX,
      y: y,
      type: 'scatter'
    }
    const data = [rettaPlot];
    Plotly.newPlot('chart-retta', data)
  
    let table = document.getElementById('table-retta');
    tableHeader(table);
    for(let i = 0; i < rettaX.length; i++){
      let newRow = table.insertRow();
      let xCell = newRow.insertCell();
      xCell.appendChild(document.createTextNode(rettaX[i]));
      let yCell = newRow.insertCell();
      yCell.appendChild(document.createTextNode(y[i]));
      let actionCell = newRow.insertCell();
      let button = document.createElement("button");
      button.classList.add("btn","btn-danger",'btn-sm','m-0','rettaDelete');
      button.setAttribute("id",rettaX[i]);
      button.setAttribute("onclick","rettaDelete("+rettaX[i]+")");
      button.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16"><path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/></svg>';
      actionCell.appendChild(button);
    }
    hideControls();
  }
  
  function rettaAdd(){
    let x = prompt('Inserisci valore x');
    if(!rettaX.includes(parseInt(x))) rettaX.push(parseInt(x));
    retta();
  }
  
  document.getElementById('input-retta-m').addEventListener('change',function(event){
    retta();
  });
  
  document.getElementById('input-retta-q').addEventListener('change',function(event){
    retta();
  });

  function rettaDelete(x){
    let index = rettaX.indexOf(x);
    if(index >= 0)
      rettaX.splice(index, 1);
      retta();
  }

  
  