const parabolaX = [];

function parabola(newX = null){
    const a = parseInt(document.getElementById('input-parabola-a').value);
    const b = parseInt(document.getElementById('input-parabola-b').value);
    const c = parseInt(document.getElementById('input-parabola-c').value);
    if(newX) parabolaX.push(parseInt(newX));
    parabolaX.sort(function(a, b) {
      return a - b;
    });;
    let y = [];
    parabolaX.forEach(val => {
      y.push(a * val * val + b * val + c); // Y = ax^2 + bx + c
    });

    const parabolaPlot = {
      x: parabolaX,
      y: y,
      type: 'scatter'
    }
    const data = [parabolaPlot];
    Plotly.newPlot('chart-parabola', data)
  
    let table = document.getElementById('table-parabola');
    tableHeader(table);
    for(let i = 0; i < parabolaX.length; i++){
      let newRow = table.insertRow();
      let xCell = newRow.insertCell();
      xCell.appendChild(document.createTextNode(parabolaX[i]));
      let yCell = newRow.insertCell();
      yCell.appendChild(document.createTextNode(y[i]));
      let actionCell = newRow.insertCell();
      let button = document.createElement("button");
      button.classList.add("btn","btn-danger",'btn-sm','m-0');
      button.setAttribute("id",parabolaX[i]);
      button.setAttribute("onclick","parabolaDelete("+parabolaX[i]+")");
      button.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16"><path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/></svg>';
      actionCell.appendChild(button);
    }
    hideControls();
  }

function parabolaAdd(){
  let x = prompt('Inserisci valore x');
  if(!parabolaX.includes(parseInt(x))) parabola(x);
}

document.getElementById('input-parabola-a').addEventListener('change',function(event){
  parabola();
});

document.getElementById('input-parabola-b').addEventListener('change',function(event){
  parabola();
});

document.getElementById('input-parabola-c').addEventListener('change',function(event){
  parabola();
});

function parabolaDelete(x){
  let index = parabolaX.indexOf(x);
  if(index >= 0)
    parabolaX.splice(index, 1);
    parabola();
}