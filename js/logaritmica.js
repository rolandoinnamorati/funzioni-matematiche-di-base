const logaritmicaX = [2,3,4];

function logaritmica(newX = null){
    const a = parseInt(document.getElementById('input-logaritmica-a').value);
    if(newX) logaritmicaX.push(parseInt(newX));
    logaritmicaX.sort(function(a, b) {
      return a - b;
    });;
    let y = [];
    logaritmicaX.forEach(val => {
      y.push(Math.log(val) / Math.log(a)); // Y = loga(x)
    });
  
    const logaritmicaPlot = {
      x: logaritmicaX,
      y: y,
      type: 'scatter'
    }
    const data = [logaritmicaPlot];
    Plotly.newPlot('chart-logaritmica', data)
  
    let table = document.getElementById('table-logaritmica');
    tableHeader(table);
      for(let i = 0; i < logaritmicaX.length; i++){
        let newRow = table.insertRow();
        let xCell = newRow.insertCell();
        xCell.appendChild(document.createTextNode(logaritmicaX[i]));
        let yCell = newRow.insertCell();
        yCell.appendChild(document.createTextNode(y[i]));
        let actionCell = newRow.insertCell();
        let button = document.createElement("button");
        button.classList.add("btn","btn-danger",'btn-sm','m-0');
        button.setAttribute("id",logaritmicaX[i]);
        button.setAttribute("onclick","logaritmicaDelete("+logaritmicaX[i]+")");
        button.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16"><path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/></svg>';
        actionCell.appendChild(button);
      }
      hideControls();
  }

  function logaritmicaAdd(){
    let x = prompt('Inserisci valore x');
    if(!logaritmicaX.includes(parseInt(x)) & x > 0) logaritmica(x);
  }
  
  document.getElementById('input-logaritmica-a').addEventListener('change',function(event){
    logaritmica();
  });

  function logaritmicaDelete(x){
    let index = logaritmicaX.indexOf(x);
    if(index >= 0)
      logaritmicaX.splice(index, 1);
      logaritmica();
  }